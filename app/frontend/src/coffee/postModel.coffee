module.exports = class Post extends Backbone.Model
  idAttribute: 'name'
  initialize: () ->

  @isImage: (url) ->
    (/\.(gif|jpg|jpeg|tiff|png)$/i).test(url)

  @isValidPost: (post) ->
    @isImage(post.url)
