global.shims =
  ES5: require("es5-shim")
  ES6: require("es6-shim")

global.$ = jQuery = window.$ = window.jQuery = require("jquery")

global._             = require("underscore")
global.Backbone      = require("../../public/assets/javascripts/backbone")
Backbone.$           = $

global.Cocktail      = require("backbone.cocktail")
Cocktail.patch(Backbone)

global.React         = require("react")
global.ReactBackbone = require("./components/react_backbone")
global.Mixins        =
  hotkey:  require("react-hotkey")

global.moment        = require("moment")
global.semantic      = require("../../public/assets/javascripts/semantic")
global.bootstrap     = require("../../public/assets/javascripts/bootstrap/bootstrap.min.js")
global.STATUS        = require("./shared/httpStatusCodes")
global.Snoocore      = require("snoocore")
global.reddit        = new Snoocore({ userAgent: 'myApp v0.0.0' })
global.KeyCode      = require("./shared/asciiKeyCodes")
