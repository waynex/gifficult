module.exports = class Orientation
  constructor: (width, height) ->
    @width = width
    @height = height

  _isPortrait: () ->
    @width < @height

  _isLandscape: () ->
    @width > @height

  _isSquare: () ->
    @width == @height

  orientation: () ->
    return 'square'    if @_isSquare()
    return 'portrait'  if @_isPortrait()
    return 'landscape' if @_isLandscape()

  equals: (width, height) ->
    temp = new @constructor(width, height)
    temp.orientation() == @orientation()

  @orientation: (width, height) ->
    orientation = new Orientation(width, height)
    orientation.orientation()
