RedditFetcher    = require("./redditFetcher")
SelfFetchingList = require("./selfFetchingList")

module.exports = Backbone.Collection.extend
  mixins: [SelfFetchingList]

  initialize: () ->
    @redditFetcher = new RedditFetcher()

  fetch: (options = {}) ->
    @redditFetcher.setProps({
      subreddit: @subreddit
      at:        options.at
      before:    options.before
      after:     options.after
    })
    @redditFetcher.fetch(
      success: (collection, response) =>
        @trigger('sfl:sync', collection, response, options)
    )
