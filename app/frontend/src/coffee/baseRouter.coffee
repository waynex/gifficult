module.exports = class BaseRouter extends Backbone.Router
  __version: '0.0.1'
  initialize: (options) ->
    @prestart(options)
    @start(options)
    @poststart(options)

  prestart: () ->
    @routeStack = ['/']

  _trackPageview: (href) ->
    if _gaq
      _gaq.push(['_trackPageview', href ])

  poststart: () ->
    @overrideATags()

  start: () ->
    Backbone.history.start({
      pushState: true
    })

  navigate: (href, options) ->
    @routeStack.push(href)
    @_trackPageview(href)
    super(href, options)

  overrideATags: () ->
    $(document).on('click', 'a', (e) =>
      $aTag = $(e.currentTarget)
      href = $aTag.attr('href')
      data = $aTag.data()
      if (!data.bypass && href?.indexOf('://') == -1)
        @navigate(href, { trigger: true })
        e.preventDefault()
        e.stopPropagation()
        return false
    )

  lastMainArticle: () ->
    _.last(_.select(@routeStack, (route) -> !route.match /^\/?i\/.*$/))

  backQuiet: () -> @navigate(@lastMainArticle(), {silent: true})
  back: () -> @navigate(@lastMainArticle(), {trigger: true})

