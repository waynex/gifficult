Footer          = require("./footer")
Orientation     = require("../orientation")
module.exports  = React.createClass
  propTypes: {
    post:     React.PropTypes.object.isRequired,
    onClick:  React.PropTypes.func.isRequired
  }

  getInitialState: () ->
    loading: true
    image: null
    error: false

  componentWillMount: () ->
    @_fetchImage()

  componentWillReceiveProps: () ->
    @_fetchImage()

  render: () ->
    if ( @state.loading )
      <div className='ui active dimmer'>
        <div
          className='ui text loader'
        >
          its gifficult to stop
        </div>
      </div>
    else if ( @state.error )
      <div> error </div>
    else
      <div>
        <a
          id='fullscreen'
          onClick={@props.onClick}
        >
          <div>
            <img
              className="image centered elevated #{@_getOrientationClass()}"
              src={@props.post.url}
            />
          </div>
          <div className='scrim'>
            <span style={fontSize: @_calculateTitleSize()}>
              {@props.post.title}
            </span>
          </div>
        </a>
        <Footer attrs=@props.post />
      </div>

  _calculateTitleSize: () ->
    "#{5 - ( @props.post.title.length / 30 )}em"

  _fetchImage: () ->
    @setState loading: true
    image         = new Image()
    image.src     = @props.post.url
    image.onload  = ()  =>
      if @state.image == image
        @setState loading: false
    image.onerror = ()  =>
      @setState error: true
    @setState image: image

  _getOrientationClass: () ->
    image_height = @state.image.height
    image_width  = @state.image.width
    Orientation.orientation(image_width, image_height)

  _frostyBG: () ->
    if @props.post.url.match(/.*\.gif$/)
      <div />
    else
      <div
        className='image frosty cover backgroundImage'
        style={backgroundImage: "url(#{@props.post.thumbnail})"}
      >
      </div>
