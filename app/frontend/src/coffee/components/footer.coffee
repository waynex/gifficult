module.exports = React.createClass
  render: () ->
    <div id='footer'>
      <i
        onClick={@_commentsClick}
        className='scrim icon comment clickable'
      >
        <span className="ui blue circular label">
          {@props.attrs.num_comments}
        </span>
      </i>
      <a
        href={"http://www.facebook.com/sharer.php?u=#{window.location.href}"}
        target='_blank'
        style={color: "#fff", textDecoration: 'none'}
      >
        <i
          className='scrim facebook sign icon clickable'
        >
        </i>
      </a>
      <a
        href={@props.attrs.url}
        download=true
        style={color: '#fff', textDecoration: 'none'}
      >
        <i
          className='scrim icon download clickable'
        >
        </i>
      </a>
    </div>

  _commentsClick: () -> window.open("http://www.reddit.com/#{@props.attrs.permalink}")

