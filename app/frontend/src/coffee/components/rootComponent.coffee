PostComponent = require("./postComponent")
Loader = require("./loader")

module.exports = React.createBackboneClass
  mixins: [Mixins.hotkey.Mixin('handleHotKey')]

  render: () ->
    if post = @props.model.get('currentNode')
      <PostComponent
        post=post.toJSON()
        onClick={@_nextPost}
      />
    else
      <Loader />

  handleHotKey: (e) ->
    switch KeyCode.is(e)
      when 'rightArrow', 'l'
        @_nextPost()
      when 'leftArrow', 'h'
        @_prevPost()

  _nextPost: () -> @props.model.trigger('nextPost')

  _prevPost: () -> @props.model.trigger('prevPost')
