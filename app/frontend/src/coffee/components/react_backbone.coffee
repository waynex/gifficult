setupModule = (root, factory) ->
  if (typeof exports == 'object')
    # CommonJS
    module.exports = factory(require('backbone'), require('react'), require('underscore'))
  else if (typeof define == 'function' && define.amd)
    # AMD. Register as an anonymous module.
    define(['backbone', 'react', 'underscore'], factory)
  else
    # Browser globals
    root.amdWeb = factory(root.Backbone, root.React, root._)

root = @

factory = (Backbone, React, _) ->
  'use strict'

  collectionBehavior = {
    changeOptions: 'add remove reset sort invalid change'
    updateScheduler: (func) ->
      return _.debounce(func, 0)
  }

  modelBehavior = {
    changeOptions: 'change invalid valid'
    # Note: if we debounce models too we can no longer use model attributes
    # as properties to react controlled components due to https://github.com/facebook/react/issues/955
    updateScheduler: _.identity
  }

  subscribe = (component, modelOrCollection, customChangeOptions) ->
    if (!modelOrCollection)
      return

    behavior = modelBehavior
    if (modelOrCollection instanceof Backbone.Collection)
      behavior = collectionBehavior

    triggerUpdate = behavior.updateScheduler(() ->
      if (component.isMounted())
        (component.onModelChange || component.forceUpdate).call(component)
    )

    changeOptions = customChangeOptions || component.changeOptions || behavior.changeOptions
    modelOrCollection.on(changeOptions, triggerUpdate, component)


  unsubscribe = (component, modelOrCollection) ->
    if (!modelOrCollection)
      return

    modelOrCollection.off(null, null, component)

  # BackboneMixin //////////////////////////////////////////////////////////////
  React.BackboneMixin = (propName, customChangeOptions) ->
    return {
      componentDidMount: () ->
        # Whenever there may be a change in the Backbone data, trigger a reconcile.
        subscribe(@, @props[propName], customChangeOptions)

      componentWillReceiveProps: (nextProps) ->
        if (@props[propName] == nextProps[propName])
          return

        unsubscribe(@, @props[propName])
        subscribe(@, nextProps[propName], customChangeOptions)

        if (typeof @componentWillChangeModel == 'function')
          @componentWillChangeModel()

      componentDidUpdate: (prevProps, prevState) ->
        if (@props[propName] == prevProps[propName])
          return

        if (typeof @componentDidChangeModel == 'function')
          @componentDidChangeModel()

      componentWillUnmount: () ->
        # Ensure that we clean up any dangling references when the component is destroyed.
        unsubscribe(@, @props[propName])
    }


  React.BackboneSessionPresenterMixin = {
    getInitialState: () ->
      attrs: {}
      session: {}

    componentWillMount: () ->
      @setState
        attrs:   @props.model.toPresenterJSON()
        session: @props.session.toPresenterJSON()

    onModelChange: () ->
      @setState
        attrs: @props.model.toPresenterJSON()
        session: @props.session.toPresenterJSON()

    componentWillReceiveProps: (nextProps) ->
      @setState
        attrs: nextProps.model.toPresenterJSON()
        session: nextProps.session.toPresenterJSON()

  }
  React.BackbonePresenterMixin = {
      getInitialState: () ->
        attrs: {}

      componentWillMount: () ->
        @setState attrs: @props.model.toPresenterJSON()

      onModelChange: () ->
        @setState attrs: @props.model.toPresenterJSON()

      componentWillReceiveProps: (nextProps) ->
        @setState attrs: nextProps.model.toPresenterJSON()
    }

  # BackboneViewMixin //////////////////////////////////////////////////////////
  React.BackboneViewMixin = {
    getModel: () ->
      return @props.model

    model: () ->
      return @getModel()

    getCollection: () ->
      return @props.collection

    collection: () ->
      return @getCollection()

    el: () ->
      return @isMounted() && @getDOMNode()
  }

  # Define createBackboneClass using mixins.
  React.createBackboneSessionPresenterClass = (spec) ->
    currentMixins = spec.mixins || []
    spec.mixins = currentMixins.concat([
      React.BackboneSessionPresenterMixin
      React.BackboneMixin('model')
      React.BackboneMixin('session')
      React.BackboneMixin('collection')
      React.BackboneViewMixin
    ])
    return React.createClass(spec)

  React.createBackbonePresenterClass = (spec) ->
    currentMixins = spec.mixins || []
    spec.mixins = currentMixins.concat([
      React.BackbonePresenterMixin
      React.BackboneMixin('model')
      React.BackboneMixin('collection')
      React.BackboneViewMixin
    ])
    return React.createClass(spec)

  React.createBackboneClass = (spec) ->
    currentMixins = spec.mixins || []
    spec.mixins = currentMixins.concat([
      React.BackboneMixin('model')
      React.BackboneMixin('collection')
      React.BackboneViewMixin
    ])
    return React.createClass(spec)

  React.PropTypes.backbone_model      = React.PropTypes.instanceOf(Backbone.Model)
  React.PropTypes.backbone_collection = React.PropTypes.instanceOf(Backbone.Model)

  return React
module.exports = setupModule(@, factory)
