DEBUG = false
module.exports = {
  initialize: () ->
    @_car = -1
    @on('sfl:next',      _.bind(@_SFLNextNode, @))
    @on('sfl:prev',      _.bind(@_SFLPrevNode, @))
    @on('sfl:edge:head', _.bind(@_SFLOnHead, @))
    @on('sfl:edge:tail', _.bind(@_SFLOnTail, @))
    @on('sfl:sync',      _.bind(@_SFLOnSync, @))

  currentNode: () ->
    @trigger('sfl:currentNode', @at(@_car))
    if DEBUG
      console.log("car #{@_car}, #{@at(@_car).get('title')}")
      console.log(" ****************")
    @at(@_car)

  _SFLOnSync: (collection, resp, options) ->
    if options.before
      @_car = collection.length
      @add(collection.models, at:0)
    else
      @_car = @length
      @add(collection.models, @length)
    console.log(@pluck('title')) if DEBUG
    @currentNode()

  _SFLOnHead: () ->
    @fetch({before: @first()?.id})

  _SFLOnTail: () ->
    @fetch({after: @last()?.id})

  _SFLNextNode: () ->
    ++@_car
    if @_car >= @length
      @trigger('sfl:edge:tail')
      @_car = @length
    else
      @currentNode()

  _SFLPrevNode: () ->
    --@_car
    if @_car <= -1
      @trigger('sfl:edge:head')
      @_car == -1
    else
      @currentNode()
}
