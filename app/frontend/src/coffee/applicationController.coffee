PostCollection = require("./postCollection")
RootComponent  = require("./components/rootComponent")
Router         = require("./router")

# Controller is a Model
# RootComponent accepts it in React.Backbone
# Creates a fluxesque relationship where data will be controlled here
# and, flow through app via root component

module.exports = class ApplicationController extends Backbone.Model
  initialize: (params = {}) ->
    @_createStores()
    @_bindEvents()
    @_start()

  updateState: (params) ->
    if params.subreddit
      @set('subreddit', "r/#{params.subreddit}/")
    @get('postCollection').subreddit = @get('subreddit')
    @get('postCollection').fetch({
      at: params.name
    })

  _start: () ->
    @set('router', new Router({controller: @}))
    React.renderComponent <RootComponent
      model={@}
    />, document.body

  _createStores: () ->
    @set('postCollection', new PostCollection())

  _bindEvents: () ->
    @get('postCollection').on('sfl:currentNode', _.bind(@_setCurrentNode, @))
    @on('nextPost', => @get('postCollection').trigger('sfl:next'))
    @on('prevPost', => @get('postCollection').trigger('sfl:prev'))

  _setCurrentNode: (currentNode) ->
    @set('currentNode', currentNode)
    @get('router').navigate("#{@get('subreddit') || ''}#{currentNode.get('name')}")
