RootComponent = require("./components/rootComponent")
BaseRouter    = require("./baseRouter")

module.exports = class Router extends BaseRouter
  prestart: (options) ->
    @AC = options.controller
    super(options)

  routes: () ->
    'r/:subreddit(/)(:name)': (subreddit, name) ->
      @_render subreddit: subreddit, name: name
    '(/)(:name)': (name) ->
      @_render subreddit: '', name: name

  _render: (params = {}) ->
    {subreddit, name} = params
    @AC.updateState(subreddit: subreddit, name: name)
