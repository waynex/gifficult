Orientation = require("./orientation")
THROTTLE_INTERVAL = 200
module.exports = Backbone.Model.extend
  initialize: () ->
    fn = _.bind(@_onWindowResize, @)
    $(window).on('resize', _.throttle(fn, THROTTLE_INTERVAL))
    @_onWindowResize()

  _onWindowResize: (e) ->
    @set 'height', $(window).height()
    @set 'width',  $(window).width()
    @set 'orientation', new Orientation(
      @get('width'),
      @get('height')
    )
