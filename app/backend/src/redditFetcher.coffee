request = require("request")

module.exports = class RedditFetcher
  constructor: (req) ->
    @_req       = req
    @_subreddit = req.params.subreddit
    @_name      = req.params.name
    @_posts = {
      keys: []
      data: {
      }
    }

  getMethod: (params) ->
    if @_name
      return "by_id/#{@_name}"
    else if @_subreddit
      return "r/#{@_subreddit}"
    else
      return ""

  url: () -> "http://www.reddit.com/#{@getMethod()}.json"

  isNeeded: (req) ->
    @_req.headers['user-agent'].toLowerCase().match /.*facebookexternalhit.*/

  isImage: (post) ->
    (/\.(gif|jpg|jpeg|tiff|png)$/i).test(post)

  isValidPost: () ->
    @isImage()

  parse: (res, body) ->
    JSON.parse(body).data.children.map (child) =>
      post = child.data
      if @isImage(post.url)
        @_posts.keys.push(post.name)
        @_posts.data[post.name] = post
    @_post = @_posts.data[@_name]
    console.log()

  json: () -> @_post

  fetch: (next) ->
    request({
      method: 'GET',
      url:    @url()
    }, (err, res, body) =>
      @parse(res, body)
      next()
    )

