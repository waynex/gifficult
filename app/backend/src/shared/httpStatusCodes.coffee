module.exports =
  INFO:
    CONTINUE:
      CODE:    100
      MESSAGE: "The server has received the request headers, and that the client should proceed to send the request body."

    SWITCHING_PROTOCOLS:
      CODE:    101
      MESSAGE: "The requester has asked the server to switch protocols and the server is acknowledging that it will do so."

    PROCESSING:
      CODE:    102
      MESSAGE: "The server has received and is processing the request, but no response is available yet."

  SUCCESS:
    OK:
      CODE:    200
      MESSAGE: "The standard response for successful HTTP requests."

    CREATED:
      CODE:    201
      MESSAGE: "The request has been fulfilled and a new resource has been created."

    ACCEPTED:
      CODE:    202
      MESSAGE: "The request has been accepted but has not been processed yet. This code does not guarantee that the request will process successfully."

    NON_AUTHORITATIVE_INFO:
      CODE:    203
      MESSAGE: "HTTP 1.1. The server successfully processed the request but is returning information from another source."

    NO_CONTENT:
      CODE:    204
      MESSAGE:  "The server accepted the request but is not returning any content. This is often used as a response to a DELETE request."

    RESET_CONTENT:
      CODE:    205
      MESSAGE: "Similar to a 204 No Content response but @  response requires the requester to reset the document view."

    PARTIAL_CONTENT:
      CODE:    206
      MESSAGE: "The server is delivering only a portion of the content, as requested by the client via a range header."

    MULTI_STATUS:
      CODE:    207
      MESSAGE: "The message body that follows is an XML message and can contain a number of separate response codes, depending on how many sub-requests were made."

    ALREADY_REPORTED:
      CODE:    208
      MESSAGE: "The members of a DAV binding have already been enumerated in a previous reply to @  request, and are not being included again."

    IM_USED:
      CODE:    226
      MESSAGE: "The server has fulfilled a GET request for the resource, and the response is a representation of the result of one or more instance-manipulations applied to the current instance."

  REDIRECTION:
    MULTIPLE_CHOICES:
      CODE:    300
      MESSAGE: "There are multiple options that the client may follow."

    MOVED_PERMANENTLY:
      CODE:    301
      MESSAGE: "The resource has been moved and all further requests should reference its new URI."

    FOUND:
      CODE:    302
      MESSAGE: "The HTTP 1.0 specification described @  status as 'Moved Temporarily', but popular browsers respond to @  status similar to behavior intended for 303. The resource can be retrieved by referencing the returned URI."

    SEE_OTHER:
      CODE:    303
      MESSAGE: "The resource can be retrieved by following other URI using the GET method. When received in response to a POST, PUT, or DELETE, it can usually be assumed that the server processed the request successfully and is sending the client to an informational endpoint."

    NOT_MODIFIED:
      CODE:    304
      MESSAGE: "The resource has not been modified since the version specified in If-Modified-Since or If-Match headers. The resource will not be returned in response body."

    USE_PROXY:
      CODE:    305
      MESSAGE: "HTTP 1.1. The resource is only available through a proxy and the address is provided in the response."

    SWITCH_PROXY:
      CODE:    306
      MESSAGE: "Deprecated in HTTP 1.1. Used to mean that subsequent requests should be sent using the specified proxy."

    TEMPORARY_REDIRECT:
      CODE:    307
      MESSAGE: "HTTP 1.1. The request should be repeated with the URI provided in the response, but future requests should still call the original URI."

    PERMANENT_REDIRECT:
      CODE:    308
      MESSAGE: "Experimental. The request and all future requests should be repeated with the URI provided in the response. The HTTP method is not allowed to be changed in the subsequent request."

  ERROR:
    CLIENT:
      BAD_REQUEST:
        CODE:    400
        MESSAGE: "The request could not be fulfilled due to the incorrect syntax of the request."

      UNAUTHORIZED:
        CODE:    401
        MESSAGE: "The requestor is not authorized to access the resource. This is similar to 403 but is used in cases where authentication is expected but has failed or has not been provided."

      PAYMENT_REQUIRED:
        CODE:    402
        MESSAGE: "Reserved for future use. Some web services use @  as an indication that the client has sent an excessive number of requests."

      FORBIDDEN:
        CODE:    403
        MESSAGE: "The request was formatted correctly but the server is refusing to supply the requested resource. Unlike 401, authenticating will not make a difference in the server's response."

      NOT_FOUND:
        CODE:    404
        MESSAGE: "The resource could not be found. This is often used as a catch-all for all invalid URIs requested of the server."

      METHOD_NOT_ALLOWED:
        CODE:    405
        MESSAGE: "The resource was requested using a method that is not allowed. For example, requesting a resource via a POST method when the resource only supports the GET method."

      NOT_ACCEPTABLE:
        CODE:    406
        MESSAGE: "The resource is valid, but cannot be provided in a format specified in the Accept headers in the request."

      PROXY_AUTHENTICATION_REQUIRED:
        CODE:    407
        MESSAGE: "Authentication is required with the proxy before requests can be fulfilled."

      REQUEST_TIMEOUT:
        CODE:    408
        MESSAGE: "The server timed out waiting for a request from the client. The client is allowed to repeat the request."

      CONFLICT:
        CODE:    409
        MESSAGE: "The request cannot be completed due to a conflict in the request parameters."

      GONE:
        CODE:    410
        MESSAGE: "The resource is no longer available at the requested URI and no redirection will be given."

      LENGTH_REQUIRED:
        CODE:    411
        MESSAGE: "The request did not specify the length of its content as required by the resource."

      PRECONDITION_FAILED:
        CODE: 412
        MESSAGE: "The server does not meet one of the preconditions specified by the client."

      REQUEST_ENTITY_TOO_LARGE:
        CODE:    413
        MESSAGE: "The request is larger than what the server is able to process."

      REQUEST_URI_TOO_LONG:
        CODE:    414
        MESSAGE: "The URI provided in the request is too long for the server to process. This is often used when too much data has been encoded into the URI of a GET request and a POST request should be used instead."

      UNSUPPORTED_MEDIA_TYPE:
        CODE:    415
        MESSAGE: "The client provided data with a media type that the server does not support."

      REQUESTED_RANGE_NOT_SATISFIABLE:
        CODE:    416
        MESSAGE: "The client has asked for a portion of the resource but the server cannot supply that portion."

      EXPECTATION_FAILED:
        CODE:    417
        MESSAGE: "The server cannot meet the requirements of the Expect request-header field."

      UNPROCESSABLE_ENTITY:
        CODE:    422
        MESSAGE: "The request was formatted correctly but cannot be processed in its current form. Often used when the specified parameters fail validation errors."

      LOCKED:
        CODE:    423
        MESSAGE: "The requested resource was found but has been locked and will not be returned."

      FAILED_DEPENDENCY:
        CODE:    424
        MESSAGE: "The request failed due to a failure of a previous request."

      UPGRADE_REQUIRED:
        CODE:    426
        MESSAGE: "The client should repeat the request using an upgraded protocol such as TLS 1.0."

    SERVER:
      GENERIC:
        CODE:    500
        MESSAGE: "A generic status for an error in the server itself."

      NOT_IMPLEMENTED:
        CODE:    501
        MESSAGE: "The server cannot respond to the request. This usually implies that the server could possibly support the request in the future — otherwise a 4xx status may be more appropriate."

      BAD_GATEWAY:
        CODE:    502
        MESSAGE: "The server is acting as a proxy and did not receive an acceptable response from the upstream server."

      SERVICE_UNAVAILABLE:
        CODE:    503
        MESSAGE: "The server is down and is not accepting requests."

      GATEWAY_TIMEOUT:
        CODE:    504
        MESSAGE: "The server is acting as a proxy and did not receive a response from the upstream server."

      HTTP_VERSION_NOT_SUPPORTED:
        CODE:    505
        MESSAGE: "The server does not support the HTTP protocol version specified in the request."

      VARIANT_ALSO_NEGOTIATES:
        CODE:    506
        MESSAGE: "Transparent content negotiation for the request results in a circular reference."

      INSUFFICIENT_STORAGE:
        CODE:    507
        MESSAGE: "The user or server does not have sufficient storage quota to fulfill the request."

      LOOP_DETECTED:
        CODE:    508
        MESSAGE: "The server detected an infinite loop in the request."

      NOT_EXTENDED:
        CODE:    510
        MESSAGE: "Further extensions to the request are necessary for it to be fulfilled."

      NETWORK_AUTHENTICATION_REQUIRED:
        CODE:    511
        MESSAGE: "The client must authenticate with the network before sending requests."
