debug        = require('debug')('here')
express      = require('express')
path         = require("path")
favicon      = require('static-favicon')
logger       = require('morgan')
cookieParser = require('cookie-parser')
bodyParser   = require('body-parser')
RedditFetcher = require("./redditFetcher")

PORT    = process.env.PORT || 41337
app     = express()

app.set('port', PORT)
app.use(favicon())
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded())
app.use(cookieParser())
# app.use(express.methodOverride())
app.use(express.static("#{__dirname}/../../frontend/public"))
app.set('views', __dirname + '/views')
app.set('view engine', 'ejs')

router = express.Router()
respond = (req, res)  ->
  reddit = new RedditFetcher(req)
  if reddit.isNeeded()
    reddit.fetch(() ->
      res.render('index', { title: 'Express', reddit: reddit.json() })
    )
  else
    res.render('index', { title: 'Express', reddit: null })
router.get '/r/:subreddit/:name?', respond
router.get '/:name?', respond
router.get '*', respond

app.use('/', router)

server = app.listen app.get('port'), ()  ->
  debug('Express server listening on port ' + server.address().port)
