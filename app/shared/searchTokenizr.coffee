validator = require("validator")
_         = require("underscore")

module.exports = class SearchTokenizr
  constructor: (input) ->
    @input = input

  tokens: () ->
    @tokencache ||= _.map(_.compact(@input.split(' ')), _.bind(@typecast, @))

  firstURL: () ->
    unless @urlcache
      dirty_url = _.filter(@tokens(), (token) -> token.type == 'url')[0]
      if dirty_url
        @urlcache = V.Utils.sanitizeURL(dirty_url.value)
    @urlcache

  typecast: (input) ->
    if validator.isURL(input)
      type  = 'url'
      value = V.Utils.sanitizeURL(input)
    else
      type  = 'string'
      value = input
    {type: type, value: value}

  visibleTokens: () ->
    if @tokens().length > 1
      _.reject(@tokens(), (token) => token.value == @firstURL())
    else
      @tokens()

  previewText: () ->
    @previewcache ||= _.map(@visibleTokens(), (token) -> token.value).join(' ')

  parse: () ->
    {raw: @input, url: @firstURL(), text: @previewText(), tokens: @tokens()}

  @parse: (input) ->
    tokenizr = new SearchTokenizr(input)
    tokenizr.parse()
