validate = require("validate.js")

validate.validators.reflects = (value, reflected_key, key, attributes) ->
  "must match #{reflected_key}" unless value == attributes[reflected_key]

module.exports = class V
  @validateBB: (object, constraints) ->
    validate(object, constraints)

  @validate: (object, constraint_key) ->
    validate(object, @CONSTRAINTS[constraint_key])

  @validateOneBB: (value, constraint_field, constraints) ->
    object = {}
    object[constraint_field] = value
    constraint = {}
    constraint[constraint_field] = constraints[constraint_field]
    errors = validate(object, constraint)
    errors?[constraint_field]

  @validateOne: (value, constraint_field, constraint_key) ->
    object = {}
    object[constraint_field] = value
    constraint = {}
    constraint[constraint_field] = @CONSTRAINTS[constraint_key][constraint_field]
    errors = validate(object, constraint)
    errors?[constraint_field]

  @hasConstraints: (key) -> @CONSTRAINTS[key]

  @Utils:
    sanitizeURL: (url) ->
      "http://#{url.replace /^http:\/\//,''}"


  @CONSTRAINTS =
    Account:
      email:
        email: true
      username:
        presence: true
      password:
        presence: true
      passwordConfirmation:
        presence: true
        reflects: 'password'
    Session:
      username:
        presence: true
      password:
        presence: true
    Group:
      name:
        presence: true
